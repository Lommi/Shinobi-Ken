/*  Copyright 2017 Juho Lommi
 *  This file is a part of Shinobi-Ken project*/
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <stdbool.h>
#include <math.h>
#include "deps/SDL2/SDL.h"
#include "deps/SDL2/SDL_image.h"

#define TARGET_FPS 20
#define SCREEN_TICK_PER_FRAME 1000 / TARGET_FPS
#define MAX_NUM_ANIMATIONS 10
#define MAX_ANIMATION_CLIPS 10
#define GRAVITY 2
#define MAX_VEL 20.0f
#define TILESIZE 32
#define NUM_TILES 40
#define VSYNC 1

typedef struct Vector Vector;
typedef struct Eangle Eangle;
typedef struct Tile Tile;
typedef struct Animation Animation;
typedef struct Player Player;

enum GameState
{
    GS_EXIT = 0,
    GS_MENU,
    GS_GAME
};

enum PlayerState
{
    STATE_IDLE = 0,
    STATE_WALK,
    STATE_RUN,
    STATE_ATTACK,
    STATE_DASHATTACK,
    STATE_DEFEND,
    STATE_JUMP
};

enum Direction
{
    DIR_LEFT = 0,
    DIR_RIGHT,
    DIR_UP,
    DIR_DOWN,
    DIR_NONE
};

struct Vector
{
    float x, y;
};

struct Eangle
{
    float y, p, r;
};

struct Tile
{
    Vector pos;
    SDL_Texture *tex;
    SDL_Rect clip;
    SDL_Rect drawquad;
    SDL_Rect collider;
};

struct Animation
{
    uint8_t  frames;
    uint8_t  lenFrame[MAX_ANIMATION_CLIPS];
    int      frameOffsetX[MAX_ANIMATION_CLIPS];
    int      frameOffsetY[MAX_ANIMATION_CLIPS];
    SDL_Rect clip[MAX_ANIMATION_CLIPS];
    SDL_Point center[MAX_ANIMATION_CLIPS];
};

struct Player
{
    bool djump, ropeActive, grounded;
    uint8_t state;
    uint8_t dirFace;            //Where player is looking
    uint8_t dirMove;            //Where player is moving
    uint8_t animFrame;          //Current animation frame number
    uint8_t animFrameCounter;   //Counts how long the frame lasts
    int width, height;
    float speedWalk;
    float speedRun;
    float speedGoal;
    float speedRope;
    float acceleration;
    Vector pos;
    Vector velocity, velRope;
    Eangle angle;

    Animation *animCurrent;
    Animation *animIdle;
    Animation *animWalk;
    Animation *animRun;
    Animation *animAttack;
    Animation *animDashAttack;
    Animation *animDefend;
    Animation *animJump;

    SDL_Texture *tex;           //Player texture spritesheet
    SDL_Rect    clipPos;        //Clip for rendering position in screen
    SDL_Rect    collider;       //Clip for collission mask
    SDL_RendererFlip flipTex;

    SDL_Point colliderpoints[5];
};

SDL_Window   *window;
SDL_Renderer *renderer;

const uint8_t *KEYSTATE;

static int gamestate           = GS_GAME;
static FILE *f;
static bool DEBUG = 0;

static uint32_t tickStart, tickCurrent, frames, frameTicks;
static float avgFps;

static Vector gravity;
static Vector mousepos;
static Player player;

static SDL_Surface *icon;
static SDL_Rect screen_size    = {0, 0, 640, 640};
static SDL_Color c_white       = {0xFF, 0xFF, 0xFF, 0xFF};
static SDL_Color c_black       = {0x00, 0x00, 0x00, 0xFF};
static SDL_Color c_red         = {0xFF, 0x00, 0x00, 0xFF};
static SDL_Color c_green       = {0x00, 0xFF, 0x00, 0xFF};
static SDL_Color c_blue        = {0x00, 0x00, 0xFF, 0xFF};
static SDL_Color c_yellow      = {0xFF, 0xFF, 0x00, 0xFF};
static SDL_Color c_purple      = {0xFF, 0x00, 0xFF, 0xFF};
static SDL_Color c_aqua        = {0x00, 0xFF, 0xFF, 0xFF};
static SDL_Point anglepoints[2];
static SDL_Texture *tileTex;

static Animation animations[MAX_NUM_ANIMATIONS];
static Animation animPlayerIdle;
static Animation animPlayerWalk;
static Animation animPlayerRun;
static Animation animPlayerAttack;
static Animation animPlayerDashAttack;
static Animation animPlayerDefend;
static Animation animPlayerJump;

static Tile tiles[NUM_TILES];

int  init();
int  shutdown();
void render();
void gameUpdate();
void input();

static inline playerIdle();
static inline playerTurnLeft();
static inline playerTurnRight();
static inline playerAttack();
static inline playerDefend();
static inline Vector vecAdd(Vector *a, Vector *b);
static inline Vector vecSub(Vector a, Vector b);
static inline void fpsCalcBegin();
static inline void fpsCalcEnd();
static inline void vecSet(Vector *v, float x, float y);
static inline void vecMul(Vector *v, float s);
static inline void vecDiv(Vector *v, float s);
static inline void vecNormalize(Vector *v);
static inline void angleToVector(Vector *v, float y, float p, float r);
static inline void angleNormalize(Eangle *a);
static inline void setTilePos(Tile *t, int x, int y);
static inline float vecLen(Vector *v);
static inline float dotProduct(Vector *a, Vector *b);
static inline float approach(float goal, float current, float s);
static inline uint32_t getTicks(uint32_t tick);

SDL_Texture *loadTexture(const char *fp);

int main(int argc, char **argv)
{
    init();
    while(gamestate)
    {
        if (!VSYNC) fpsCalcBegin();
        input();
        gameUpdate();
        render();
        if (!VSYNC) fpsCalcEnd();
    }
    shutdown();

    return 0;
}

int init()
{
    window       = 0;
    renderer     = 0;

    srand(time(0));

    if (SDL_Init( SDL_INIT_EVERYTHING ) < 0){ printf("SDL_Init failure, SDL_Error: %s\n", SDL_GetError()); exit(0); }

    window = SDL_CreateWindow("",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        screen_size.w, screen_size.h, SDL_WINDOW_SHOWN);

    if (!window){ printf("SDL_CreateWindow failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    if (VSYNC)
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    else
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (!renderer){ printf("SDL_CreateRenderer failure, SDL Error: %s\n", SDL_GetError()); exit(0); }

    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) { printf("SDL_Image init failure, SDL_Image Error: %s\n", IMG_GetError()); exit(0); }

	if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		printf( "Warning: Linear texture filtering not enabled!" );

    tickStart = SDL_GetTicks();

    icon = IMG_Load("../assets/tmp_icon.ico");
    SDL_SetWindowIcon(window, icon);
    SDL_FreeSurface(icon);

    SDL_ShowCursor(SDL_ENABLE);

    tileTex = loadTexture("../assets/tile.png");

    for (int i = 0; i < NUM_TILES; ++i)
    {
        if (i < 20)
            tiles[i].pos.y = i * TILESIZE;
        else
        {
            tiles[i].pos.x = (i - 20) * TILESIZE;
            tiles[i].pos.y = screen_size.h - TILESIZE;
        }
        tiles[i].tex = tileTex;
        tiles[i].collider.x = tiles[i].pos.x;
        tiles[i].collider.y = tiles[i].pos.y;
        tiles[i].collider.w = tiles[i].pos.x + TILESIZE;
        tiles[i].collider.h = tiles[i].pos.y + TILESIZE;
        tiles[i].drawquad.x = tiles[i].pos.x;
        tiles[i].drawquad.y = tiles[i].pos.y;
        tiles[i].drawquad.w = TILESIZE;
        tiles[i].drawquad.h = TILESIZE;
    }

    setTilePos(&tiles[0], 6, 15);
    setTilePos(&tiles[1], 7, 15);
    setTilePos(&tiles[2], 10, 18);

    animPlayerIdle.frames = 1;
    animPlayerIdle.clip[0].x = 0;
    animPlayerIdle.clip[0].y = 0;
    animPlayerIdle.clip[0].w = 48;
    animPlayerIdle.clip[0].h = 64;
    animPlayerIdle.lenFrame[0] = 1;
    animPlayerIdle.frameOffsetX[0] = -8;

    animPlayerWalk.frames = 4;
    for (int i = 0; i < animPlayerWalk.frames; ++i)
    {
        animPlayerWalk.clip[i].x = 48 + (i * 36);
        animPlayerWalk.clip[i].y = 0;
        animPlayerWalk.clip[i].w = 36;
        animPlayerWalk.clip[i].h = 64;
        animPlayerWalk.lenFrame[i] = 8;
        animPlayerWalk.frameOffsetX[0] = -3;
    }

    animPlayerRun.frames = 4;
    for (int i = 0; i < animPlayerRun.frames; ++i)
    {
        animPlayerRun.clip[i].x = 48 + (i * 36);
        animPlayerRun.clip[i].y = 0;
        animPlayerRun.clip[i].w = 36;
        animPlayerRun.clip[i].h = 64;
        animPlayerRun.lenFrame[i] = 4;
        animPlayerRun.frameOffsetX[0] = -3;
    }

    animPlayerAttack.frames = 1;
    animPlayerAttack.clip[0].x = 102;
    animPlayerAttack.clip[0].y = 64;
    animPlayerAttack.clip[0].w = 48;
    animPlayerAttack.clip[0].h = 64;
    animPlayerAttack.lenFrame[0] = 1;
    animPlayerAttack.frameOffsetX[0] = -8;

    animPlayerDashAttack.frames = 3;
    animPlayerDashAttack.clip[0].x = 56;
    animPlayerDashAttack.clip[1].x = 118;
    animPlayerDashAttack.clip[2].x = 176;
    animPlayerDashAttack.clip[3].x = 176;
    for (int i = 0; i < animPlayerDashAttack.frames; ++i)
    {
        animPlayerDashAttack.clip[i].y = 196;
        animPlayerDashAttack.clip[i].w = 60;
        animPlayerDashAttack.clip[i].h = 80;
        animPlayerDashAttack.frameOffsetY[i] = -12;
        animPlayerDashAttack.lenFrame[i] = 4;
    }

    animPlayerDashAttack.frameOffsetX[0] = -12;
    animPlayerDashAttack.frameOffsetX[1] = -16;
    animPlayerDashAttack.frameOffsetX[2] = -24;
    animPlayerDashAttack.clip[2].w = 80;

    animPlayerDefend.frames = 1;
    animPlayerDefend.clip[0].x = 176;
    animPlayerDefend.clip[0].y = 128;
    animPlayerDefend.clip[0].w = 48;
    animPlayerDefend.clip[0].h = 64;
    animPlayerDefend.lenFrame[0] = 1;
    animPlayerDefend.frameOffsetX[0] = -8;

    animPlayerJump.frames = 4;
    for (int i = 0; i < animPlayerJump.frames; ++i)
    {
        animPlayerJump.clip[i].x = 178 + (i * 36);
        animPlayerJump.clip[i].y = 312;
        animPlayerJump.clip[i].w = 36;
        animPlayerJump.clip[i].h = 64;
        animPlayerJump.lenFrame[i] = 2;
    }

    player.tex = loadTexture("../assets/ken.png");
    player.pos.x   = 0;
    player.pos.y   = 300;

    player.animIdle = &animPlayerIdle;
    player.animWalk = &animPlayerWalk;
    player.animRun = &animPlayerRun;
    player.animAttack = &animPlayerAttack;
    player.animDashAttack = &animPlayerDashAttack;
    player.animDefend = &animPlayerDefend;
    player.animJump = &animPlayerJump;
    player.animCurrent = player.animIdle;
    player.animFrame = 0;
    player.animFrameCounter = 0;

    player.clipPos.x = player.pos.x;
    player.clipPos.y = player.pos.y;
    player.clipPos.w = player.animCurrent->clip[0].w;
    player.clipPos.h = player.animCurrent->clip[0].h;

    player.width = 32;
    player.height = 64;

    player.dirFace = DIR_RIGHT;
    player.dirMove = DIR_NONE;
    player.flipTex = SDL_FLIP_NONE;
    player.speedWalk = 4.0f;
    player.speedRun = 8.0f;
    player.speedRope = 0.5f;
    player.acceleration = 0.5f;
    player.state = STATE_IDLE;

    //main_font  = loadFont("../assets/OrbitronMedium.TTF", 24);

    vecSet(&gravity, 0, 1);

    return 0;
}

int shutdown()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window   = 0;
    renderer = 0;
    IMG_Quit();
    SDL_Quit();

    return 0;
}

void render()
{
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(renderer);

    switch (gamestate)
    {
        case GS_GAME:
        {
            player.clipPos.x = player.pos.x +
                player.animCurrent->frameOffsetX[player.animFrame];
            player.clipPos.y = player.pos.y +
                player.animCurrent->frameOffsetY[player.animFrame];
            player.clipPos.w = player.animCurrent->clip[player.animFrame].w;
            player.clipPos.h = player.animCurrent->clip[player.animFrame].h;
            SDL_RenderCopyEx(renderer, player.tex,
            &player.animCurrent->clip[player.animFrame],
            &player.clipPos, 0, &player.animCurrent->center[player.animFrame],
            player.flipTex);

            for (int i = 0; i < NUM_TILES; ++i)
                SDL_RenderCopy(renderer, tiles[i].tex,
                    0, &tiles[i].drawquad);

            SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
            if (DEBUG)
                SDL_RenderDrawLines(renderer, player.colliderpoints, 5);
            if (player.ropeActive)
                SDL_RenderDrawLines(renderer, anglepoints, 2);
        }
        case GS_MENU:
        default: break;
    }

    SDL_RenderPresent(renderer);
}

void gameUpdate()
{
    switch (gamestate)
    {
        case GS_GAME:
        {
            player.grounded = 0;
            ++player.animFrameCounter;
            if (player.animFrameCounter >=
                player.animCurrent->lenFrame[player.animFrame])
            {
                ++player.animFrame;
                if (player.animFrame >= player.animCurrent->frames)
                {
                    if (player.state == STATE_DASHATTACK)
                    {
                        player.state = STATE_IDLE;
                        player.animCurrent = player.animIdle;
                    }
                    player.animFrame = 0;
                }
                player.animFrameCounter = 0;
            }
            if (player.dirMove == DIR_RIGHT)
            {
                if (player.state == STATE_WALK)
                    player.speedGoal = player.speedWalk;
                else if (player.state == STATE_RUN)
                    player.speedGoal = player.speedRun;
            }
            if (player.dirMove == DIR_LEFT)
            {
                if (player.state == STATE_WALK)
                    player.speedGoal = -player.speedWalk;
                else if (player.state == STATE_RUN)
                    player.speedGoal = -player.speedRun;
            }

            if (player.ropeActive)
            {
                player.velRope = vecSub(mousepos, player.pos);
                vecNormalize(&player.velRope);
                vecMul(&player.velRope, player.speedRope);
                vecAdd(&player.velocity, &player.velRope);
            }
            else
                player.velocity.x = approach(player.speedGoal, player.velocity.x, player.acceleration);


            if (!player.grounded && !player.ropeActive) vecAdd(&player.velocity, &gravity);

            vecAdd(&player.pos, &player.velocity);

            if (player.velocity.x > MAX_VEL) player.velocity.x = MAX_VEL;
            if (player.velocity.x < -MAX_VEL) player.velocity.x = -MAX_VEL;
            if (player.velocity.y > MAX_VEL) player.velocity.y = MAX_VEL;
            if (player.velocity.y < -MAX_VEL) player.velocity.y = -MAX_VEL;

            player.collider.x = player.pos.x;
            player.collider.y = player.pos.y;
            player.collider.w = player.pos.x + player.width;
            player.collider.h = player.pos.y + player.height;

            for (int i = 0; i < NUM_TILES; ++i)
            {
                if (player.collider.x < tiles[i].collider.w &&
                    player.collider.w > tiles[i].collider.x &&
                    player.collider.y < tiles[i].collider.h &&
                    player.collider.h > tiles[i].collider.y)
                    {
                        if (player.pos.y + (player.height / 2) >= tiles[i].collider.y)
                        {
                            if (player.collider.x + (player.width / 2) < tiles[i].collider.x + (TILESIZE / 2))
                                player.pos.x = tiles[i].collider.x - player.width;
                            else
                                player.pos.x = tiles[i].collider.w;
                        }
                        else if (player.velocity.y > 0)
                        {
                            player.pos.y = tiles[i].collider.y - player.height;
                            player.grounded = 1;
                        }
                    }
            }

            if (player.grounded)
            {
                vecSet(&player.velocity, player.velocity.x, 0);
                player.djump = 1;
                if (player.state == STATE_JUMP)
                {
                    player.state = STATE_IDLE;
                    player.animCurrent = player.animIdle;
                }
            }

            if (player.collider.x < 0) player.pos.x = 0;
            if (player.collider.w > screen_size.w) player.pos.x = screen_size.w - player.width;
            if (player.collider.h > screen_size.h) player.pos.y = screen_size.h - player.height;

            if (player.ropeActive)
            {
                anglepoints[0].x = player.pos.x + 16;
                anglepoints[0].y = player.pos.y + 28;
                anglepoints[1].x = mousepos.x;
                anglepoints[1].y = mousepos.y;
            }

            if (DEBUG)
            {
                player.colliderpoints[0].x = player.pos.x;
                player.colliderpoints[0].y = player.pos.y;
                player.colliderpoints[1].x = player.collider.w;
                player.colliderpoints[1].y = player.pos.y;
                player.colliderpoints[2].x = player.collider.w;
                player.colliderpoints[2].y = player.collider.h;
                player.colliderpoints[3].x = player.pos.x;
                player.colliderpoints[3].y = player.collider.h;
                player.colliderpoints[4].x = player.pos.x;
                player.colliderpoints[4].y = player.pos.y;
            }
        }break;
        case GS_MENU:
        default: break;
    }
}

void input()
{
    KEYSTATE = SDL_GetKeyboardState(NULL);

    if (!KEYSTATE[SDL_SCANCODE_D] && !KEYSTATE[SDL_SCANCODE_A] &&
        player.state != STATE_ATTACK && player.state != STATE_DEFEND &&
        player.state != STATE_DASHATTACK && player.state != STATE_JUMP)
        playerIdle();
    if (KEYSTATE[SDL_SCANCODE_A] && !KEYSTATE[SDL_SCANCODE_D] &&
        player.state != STATE_ATTACK && player.state != STATE_DEFEND &&
        player.state != STATE_DASHATTACK && player.state != STATE_JUMP)
        playerTurnLeft();
    if (KEYSTATE[SDL_SCANCODE_D] && !KEYSTATE[SDL_SCANCODE_A] &&
        player.state != STATE_ATTACK && player.state != STATE_DEFEND &&
        player.state != STATE_JUMP)
        playerTurnRight();

    SDL_Event e;
    while(SDL_PollEvent(&e) != 0)
    {
        switch (e.type)
        {
            case (SDL_QUIT): gamestate = GS_EXIT; break;
            case (SDL_KEYDOWN):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                    {
                        gamestate = GS_EXIT;
                    } break;
                    case SDLK_LEFT:
                    case SDLK_a:
                    {
                        playerTurnLeft();
                    }break;
                    case SDLK_RIGHT:
                    case SDLK_d:
                    {
                        playerTurnRight();
                    }break;
                    case SDLK_j:
                    {
                        playerAttack();
                    }break;
                    case SDLK_k:
                    {
                        playerDefend();
                    }break;
                    case SDLK_SPACE:
                    case SDLK_w:
                    {
                        if (player.grounded || player.djump)
                        {
                            if (!player.grounded &&
                                player.djump) player.djump = 0;
                            player.ropeActive = 0;
//                            vecSet(&player.pos, player.pos.x,
 //                               player.pos.y - 8);
                            vecSet(&player.velocity, player.velocity.x,
                                -15);
                            player.state = STATE_JUMP;
                            player.animCurrent = player.animJump;
                            player.animFrame = 0;
                            player.animFrameCounter = 0;
                        }
                    }break;
                    case SDLK_1:
                    {
                        DEBUG = DEBUG ? 0 : 1;
                    }break;
                }
            }break;
            case (SDL_KEYUP):
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_j:
                    {
                        if (player.state != STATE_DEFEND &&
                            player.state == STATE_ATTACK)
                            playerIdle();
                    }break;
                    case SDLK_k:
                    {
                        if (player.state != STATE_ATTACK &&
                            player.state == STATE_DEFEND)
                            playerIdle();
                    }break;
                }
            }break;
            case (SDL_MOUSEBUTTONDOWN):
            {
                if (player.collider.h >= screen_size.h)
                {
                    vecSet(&player.pos, player.pos.x, player.pos.y - 1.0f);
                }
                mousepos.x = e.button.x;
                mousepos.y = e.button.y;

                player.ropeActive = 1;
                player.djump = 1;
                player.speedGoal = 0.0f;
            }break;
            case (SDL_MOUSEBUTTONUP):
            {
                player.ropeActive = 0;
            }break;
        }
    }
    SDL_PumpEvents();
}

SDL_Texture *loadTexture(const char *fp)
{
    SDL_Texture *newtex = 0;
    SDL_Surface *surface = IMG_Load(fp);
    if (!surface) printf("Image load failed! %s\n", IMG_GetError());
    else
    {
        newtex = SDL_CreateTextureFromSurface(renderer, surface);
        if (!newtex) printf("Texture creation failed! %s\n", SDL_GetError());
        SDL_FreeSurface(surface);
    }
    return newtex;
}

static inline void fpsCalcBegin()
{
    tickCurrent = SDL_GetTicks();
    avgFps = frames / (getTicks(tickStart) / 1000.0f);
    if (avgFps > 2000000) avgFps = 0;
    if (DEBUG) printf("avgFps: %f\n", avgFps);
}

static inline void fpsCalcEnd()
{
    ++frames;
    frameTicks = getTicks(tickCurrent);
    if (frameTicks < SCREEN_TICK_PER_FRAME)
        SDL_Delay(SCREEN_TICK_PER_FRAME - frameTicks);
}

static inline playerIdle()
{
    player.state = STATE_IDLE;
    player.dirMove = DIR_NONE;
    player.animCurrent = player.animIdle;
    player.speedGoal = 0.0f;
}
static inline playerTurnLeft()
{
    if (player.state != STATE_ATTACK &&
        player.state != STATE_DEFEND &&
        player.state != STATE_DASHATTACK)
    {
        player.dirFace = DIR_LEFT;
        player.dirMove = DIR_LEFT;
        player.flipTex = SDL_FLIP_HORIZONTAL;
        if (KEYSTATE[SDL_SCANCODE_LSHIFT])
        {
            player.animCurrent = player.animRun;
            player.state = STATE_RUN;
        }
        else
        {
            player.animCurrent = player.animWalk;
            player.state = STATE_WALK;
        }
    }
}
static inline playerTurnRight()
{
    if (player.state != STATE_ATTACK &&
        player.state != STATE_DEFEND &&
        player.state != STATE_DASHATTACK)
    {
        player.dirFace = DIR_RIGHT;
        player.dirMove = DIR_RIGHT;
        player.flipTex = SDL_FLIP_NONE;
        if (KEYSTATE[SDL_SCANCODE_LSHIFT])
        {
            player.animCurrent = player.animRun;
            player.state = STATE_RUN;
        }
        else
        {
            player.animCurrent = player.animWalk;
            player.state = STATE_WALK;
        }
    }
}

static inline playerAttack()
{
    player.dirMove = DIR_NONE;
    player.speedGoal = 0.0f;
    if (player.state != STATE_RUN)
    {
        player.state = STATE_ATTACK;
        player.animCurrent = player.animAttack;
    }
    else
    {
        player.state = STATE_DASHATTACK;
        player.animCurrent = player.animDashAttack;
    }
    player.animFrame = 0;
    player.animFrameCounter = 0;
}

static inline playerDefend()
{
    player.dirMove = DIR_NONE;
    player.state = STATE_DEFEND;
    player.speedGoal = 0.0f;
    player.animFrame = 0;
    player.animFrameCounter = 0;
    player.animCurrent = player.animDefend;
}

static inline Vector vecAdd(Vector *a, Vector *b)
{
    Vector add;
    add.x = a->x += b->x;
    add.y = a->y += b->y;
    return add;
}
static inline Vector vecSub(Vector a, Vector b)
{
    Vector sub;
    sub.x = a.x -= b.x;
    sub.y = a.y -= b.y;
    return sub;
}

static inline void vecSet(Vector *v, float x, float y)
{
    v->x = x;
    v->y = y;
}

static inline void vecMul(Vector *v, float s)
{
    v->x *= s;
    v->y *= s;
}

static inline void vecDiv(Vector *v, float s)
{
    v->x /= s;
    v->y /= s;
}

static inline void vecNormalize(Vector *v)
{
    float len = vecLen(v);
    vecDiv(v, len);
}

static inline void setTilePos(Tile *t, int x, int y)
{
    t->pos.x = x * TILESIZE;
    t->pos.y = y * TILESIZE;
    t->collider.x = t->pos.x;
    t->collider.y = t->pos.y;
    t->collider.w = t->pos.x + TILESIZE;
    t->collider.h = t->pos.y + TILESIZE;
    t->drawquad.x = t->pos.x;
    t->drawquad.y = t->pos.y;
    t->drawquad.w = TILESIZE;
    t->drawquad.h = TILESIZE;
}

static inline float vecLen(Vector *v)
{
    float len;
    len = sqrt(v->x * v->x + v->y * v->y);
    return len;
}

static inline float dotProduct(Vector *a, Vector *b)
{
    return (a->x * b->x) + (a->y * b->y);
}

static inline float approach(float goal, float current, float s)
{
    float difference = goal - current;

    if (difference > s)
        return current + s;
    if (difference < -s)
        return current - s;

    return goal;
}

static inline uint32_t getTicks(uint32_t tick)
{
    uint32_t t = 0;
    t = SDL_GetTicks() - tick;
    return t;
}

static inline void angleToVector(Vector *v, float y, float p, float r)
{
    v->x = cos(y)*cos(p);
    v->y = sin(p);
    //v->z = sin(y)*cos(p);
}

static inline void angleNormalize(Eangle *a)
{
    if (a->p > 89) a->p = 89;
    if (a->p < -89) a->p = -89;

    while (a->y < -180) a->y += 360;
    while (a->y > 180) a->y -= 360;
}
