CC = cl
RM = del
TARGETS = main
OBJECTS = build\intermediate\*.obj
COMPILER_FLAGS_D = /D_WANGBLOWS /D_DEBUG /wd4142 /I $(INCLUDE_PATH) /Wall /EHsc /c /Zi /Od /Tc
COMPILER_FLAGS = /D_WANGBLOWS /wd4142 /I $(INCLUDE_PATH) /Wall /EHsc /c /Ox /Tc
LINKER_FLAGS_D = /SUBSYSTEM:CONSOLE
LINKER_FLAGS = /SUBSYSTEM:WINDOWS
LIBS = SDL2main.lib SDL2.lib SDL2_image.lib SDL2_ttf.lib SDL2_mixer.lib
EXE_NAME = Shinobi-Ken.exe

.default: all

.PHONY: run

all: $(TARGETS) link

link:
	LINK $(LINKER_FLAGS_D) $(OBJECTS) $(LIBS) /OUT:build\$(EXE_NAME)

main:
	$(CC) $(COMPILER_FLAGS_D) src\main.c /Fobuild\intermediate\main.obj 

run:
	cd build && $(EXE_NAME)
